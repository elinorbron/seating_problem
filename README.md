# The Seating Problem


## Install Instructions

- Open a command prompt.
- Navigate to your dev directory.
- Run `git clone git@gitlab.com:elinorbron/seating_problem.git`

---

- Open an `Anaconda Prompt` with **Administrator Access**.
- `cd` into the project directory.
- Run `conda env create`

---

- Launch **Visual Studio Code** and open the project directory.
- Select the correct Python environment.
  - `Ctl+Shft+P` to open the command pallete.
  - Type `psi` and select the `Python: Select Interpreter` command.
  - Choose the `seating_problem` environment from the list.

---
