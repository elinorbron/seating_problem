import itertools
import logging
from pathlib import Path
from time import time

import numpy as np
import pandas as pd
import pyomo
import pyomo.environ as pyo
import pyomo.opt

logging.basicConfig(format="%(asctime)s : %(levelname)s : %(message)s", level=logging.INFO)


DATA_DIR = "comp_data"


class SeatingProblem:
    """This class implements an optimization problem of seat assignment to employees in a company.

        It takes as input five csv files, providing data for the seats, employess, hierarchy of the company,
        collaborationbetween employees, physical distance between seat, assistants information and fixed seats.
        The files are described in an attached document.

        Node, Imbalance
        that specify the node name and the flow imbalance at the node.  The arcs file should have columns:
        Start, End, Cost, UpperBound, LowerBound
        that specify an arc start node, an arc end node, a cost for the arc, and upper and lower bounds for the flow."""


    def __init__(self, DATA_DIR, subset=True, desired_num_of_people=10):
        ## pandas manipulations to get the data into the desired form
        df = self.read_sitting_data_dir(DATA_DIR)
        df = self.clean_df_set_cols(df)
        if subset:
            df = self.create_subset_of_data(df, desired_num_of_people)

        self.people_data = df["peops"][["needs_focus"]]
        self.people_data.sort_index(inplace=True)

        self.subordination_data = (
            df["peops"]["reports_to"].reset_index().set_index(["email", "reports_to"])
        )
        self.subordination_data.sort_index(inplace=True)

        self.assistant_data = df["assistant"].reset_index().set_index(["assistant", "executive"])
        self.assistant_data.sort_index(inplace=True)

        self.collab_data = df["collaboration"].stack()
        self.collab_data.sort_index(inplace=True)

        self.dist_data = (
            df["dist"].rename(columns={"Unnamed: 0": "geo_id"}).set_index("geo_id").stack()
        )
        self.dist_data.sort_index(inplace=True)

        tmp = df["dist"].rename(columns={"Unnamed: 0": "geo_id"}).set_index("geo_id")
        df["seats"]["min"] = tmp[tmp != 0].min()
        self.seats_data = df["seats"][["Function", "Concentration", "Attractiveness", "min"]]
        self.seats_data.sort_index(inplace=True)

        self.seats_set = self.seats_data.index.unique()
        self.people_set = self.people_data.index.unique()
        self.subordination_set = self.subordination_data.index.unique()
        self.collab_set = self.collab_data.index.unique()
        self.dist_set = self.dist_data.index.unique()
        self.assistant_set = self.assistant_data.index.unique()

        print(f"number of employees: {len(self.people_set)}")
        print(f"number of seats: {len(self.seats_set)}")

        self.assignments_data = self.define_assignments_set(df)
        self.assignments_set = self.assignments_data.index.unique()

        self.collab_ass_data = self.define_collaboration_assignment_set(df)
        self.collab_ass_set = self.collab_ass_data.index.unique()

        self.createModel()

    def createModel(self):

        """Create the pyomo model given the csv data."""
        self.m = pyo.ConcreteModel()

        # Create sets
        self.m.seats_set = pyo.Set(initialize=self.seats_set)
        self.m.people_set = pyo.Set(initialize=self.people_set)
        self.m.subordination_set = pyo.Set(initialize=self.subordination_set, dimen=2)
        self.m.collab_set = pyo.Set(initialize=self.collab_set, dimen=2)
        self.m.dist_set = pyo.Set(initialize=self.dist_set, dimen=2)
        self.m.assistant_set = pyo.Set(initialize=self.assistant_set, dimen=2)
        self.m.assignments_set = pyo.Set(initialize=self.assignments_set, dimen=2)
        self.m.collab_ass_set = pyo.Set(initialize=self.collab_ass_set, dimen=4)

        # Create variables
        self.m.Z = pyo.Var(
            self.m.collab_ass_set, within=pyo.Binary, initialize=0
        )  # pyo.NonNegativeReals

        # Create objective
        def obj1(m):
            return sum(
                m.Z[(s_p[0], s_p[1], s_p[0], s_p[1])]
                * self.seats_data.loc[s_p[0], "Concentration"]
                * self.people_data.loc[s_p[1], "needs_focus"]
                for s_p in self.assignments_set
            )

        def obj2(m):
            collaboration_penalty = 0

            for ia, ib in itertools.combinations(self.people_set, 2):
                coll = self.collab_data[(ia, ib)]
                dist = 0
                for ja, jb in itertools.combinations(self.seats_set, 2):
                    dist += m.Z[(ja, ia, jb, ib)] * self.dist_data[(ja, jb)]
                    collaboration_penalty += coll * dist
            return collaboration_penalty

        def obj_rule(m):
            return obj1(m) + obj2(m)

        self.m.OBJ = pyo.Objective(rule=obj_rule, sense=pyo.minimize)

        # create constraints

        def fixed(m, s, p):
            s_p = (s, p)
            return m.Z[(s, p, s, p)] - self.assignments_data.loc[s_p, "Fixed"] >= 0

        self.m.Fixed = pyo.Constraint(self.m.assignments_set, rule=fixed)

        def one_per_chair(m, s):
            return (
                sum(
                    m.Z[(s, p0, s, p1)] for (p0, p1) in itertools.product(self.people_set, repeat=2)
                )
                <= 1
            )

        self.m.OnePerChair = pyo.Constraint(self.m.seats_set, rule=one_per_chair)

        def subordination(m, p0, p1):
            return (
                sum(
                    (
                        self.seats_data.loc[s1, "Attractiveness"]
                        - self.seats_data.loc[s0, "Attractiveness"]
                    )
                    * (m.Z[(s0, p0, s1, p1)])
                    for (s0, s1) in itertools.product(self.seats_set, repeat=2)
                )
                >= 0
            )

        self.m.Subordination = pyo.Constraint(self.m.subordination_set, rule=subordination)

        def z_symmetry(m, s0, p0, s1, p1):
            return m.Z[(s0, p0, s1, p1)] - m.Z[(s1, p1, s0, p0)] == 0

        self.m.Z_symmetry = pyo.Constraint(self.m.collab_ass_set, rule=z_symmetry)

        # every pair (in order) is assigned to exactly one seating arrangement
        def every_pair_seats(m, p0, p1):
            return (
                sum(
                    m.Z[(s0, p0, s1, p1)]
                    for (s0, s1) in itertools.product(self.seats_set, repeat=2)
                )
                == 1
            )

        self.m.Every_pair_seats = pyo.Constraint(self.m.collab_set, rule=every_pair_seats)

        # If I don't sit sin seat s, all combinations of me with seat s must be zero.
        def consistency(m, s, p):
            return sum(
                m.Z[(s, p, s1, p1)]
                for (s1, p1) in itertools.product(self.seats_set, self.people_set)
            ) <= m.Z[(s, p, s, p)] * len(self.people_set)

        self.m.Consistency = pyo.Constraint(self.m.assignments_set, rule=consistency)

        # assistant seat closest
        def assistant_adjacency(m, se, e, sa, a):
            if (a, e) not in self.m.assistant_set:
                return pyo.Constraint.Skip
            # if this is the seating arrangement, must conform to the rules
            return (
                None,
                m.Z[(se, e, sa, a)] * self.dist_data[(se, sa)]
                - m.Z[(se, e, sa, a)] * self.seats_data.loc[se, "min"],
                0,
            )

        self.m.Assistant_adjacency = pyo.Constraint(self.m.collab_ass_set, rule=assistant_adjacency)


    def solve(self, local=True, solvername=["glpk", "scipampl", "cplex"][0]):
        """Solve the model."""
        if local:
            solver = pyomo.opt.SolverFactory(solvername)
            results = solver.solve(self.m, tee=True, keepfiles=True)
        else:
            solver = pyo.SolverManagerFactory("neos")
            results = solver.solve(self.m, opt=solvername)

        if results.solver.status != pyomo.opt.SolverStatus.ok:
            logging.warning("Check solver not ok?")
        if results.solver.termination_condition != pyomo.opt.TerminationCondition.optimal:
            logging.warning("Check solver optimality?")


    def read_sitting_data_dir(self, dir_name=DATA_DIR):  # data specific
        PATHS = {
            "assistant": "assistants.csv",
            "collaboration": "coll_mat.csv",
            "dist": "dist.csv",
            "peops": "people_data.csv",
            "seats": "spaces_data.csv",
        }
        df = {}
        for key, file_path in PATHS.items():
            df[key] = pd.read_csv(Path.cwd() / dir_name / file_path)
        return df


    def clean_df_set_cols(self, df):  # data specific
        df["peops"] = df["peops"].dropna(axis=0, how="all").set_index("email").iloc[:, 0:2]
        df["collaboration"] = (
            df["collaboration"].fillna(0).rename(columns={"Unnamed: 0": "email"}).set_index("email")
        )
        df["assistant"] = df["assistant"].dropna(axis=0, how="all")
        df["seats"] = df["seats"].set_index("geo_id").dropna(axis=0, how="all")
        seatable = (df["seats"].Function == "Cubicle") | (df["seats"].Function == "Office")
        df["seats"] = df["seats"][seatable]
        df["peops"]["needs_focus"] = df["peops"]["workstyle"] == "Concentrator"
        df["peops"]["needs_focus"] = df["peops"]["needs_focus"].astype("int")
        df["seats"]["Concentration"] = df["seats"]["Concentration"] < 3
        df["seats"]["Concentration"] = df["seats"]["Concentration"].astype("int")
        return df


    def create_subset_of_data(self, df, desired_num_of_people):
        chosen_people = []
        old = df["peops"]
        nk = desired_num_of_people + old.shape[0] // 8

        while len(chosen_people) <= desired_num_of_people:
            df["peops"] = old.sample(n=nk, replace=False, random_state=1).sort_index()
            chosen_people = df["peops"].index.to_list()
            while True:
                old_chosen_people = chosen_people
                df["peops"] = df["peops"][
                    df["peops"]["reports_to"].apply(lambda x: x in chosen_people)
                ]
                chosen_people = df["peops"].index.to_list()  # less people now
                if old_chosen_people == chosen_people:
                    break

        df["assistant"] = df["assistant"][
            df["assistant"].assistant.apply(lambda x: x in chosen_people)
            & df["assistant"].executive.apply(lambda x: x in chosen_people)
        ]
        df["collaboration"] = df["collaboration"].loc[chosen_people, chosen_people]

        df["seats"] = df["seats"][
            (df["seats"].Fixed != "yes")
            | (df["seats"]["Current assignment"].apply(lambda x: x in chosen_people))
        ]
        df["seats"] = (
            df["seats"].sample(n=len(chosen_people) + 3, replace=False, random_state=1).sort_index()
        )
        return df


    def define_assignments_set(self, df):
        assignments = pd.DataFrame(
            np.zeros((len(self.seats_set), len(self.people_set)), dtype=float),
            index=self.seats_set,
            columns=self.people_set,
        )
        assignments = pd.DataFrame(assignments.stack(), columns=["assigned"])
        fixed_pairs = (
            df["seats"]["Current assignment"][df["seats"].Fixed == "yes"]
            .reset_index()
            .set_index(["geo_id", "Current assignment"])
            .index.tolist()
        )
        assignments["Fixed"] = 0
        mask_fixed = assignments["Fixed"].index.isin(fixed_pairs)
        assignments["Fixed"].mask(mask_fixed, 1, inplace=True)
        assignments.sort_index(inplace=True)
        return assignments


    def define_collaboration_assignment_set(self, df):
        collab_ass = pd.DataFrame(
            np.zeros((len(self.assignments_set), len(self.assignments_set)), dtype=float),
            index=self.assignments_set,
            columns=self.assignments_set,
        )
        collab_ass = pd.DataFrame(collab_ass.stack(0).stack(), columns=["assigned"])
        collab_ass.sort_index(inplace=True)
        return collab_ass


    def print_res(self, versbose=False):
        print("\n\n---------------------------")
        print("Cost: ", self.m.OBJ())
        for var, val in self.m.Z.get_values().items():
            if val == 1:
                if var[0] == var[2] and var[1] == var[3]:
                    print(f"{var[0]}: {var[1]}")
        print("----------------------------------")
        if versbose:
            print("Other variables")
            for var, val in sp.m.Z.get_values().items():
                if val == 1:
                    if var[0] != var[2] and var[1] != var[3]:
                        print(var)


if __name__ == "__main__":
    # generates problem instance from the complete data\subset of the data found in the folder DATA_DIR
    sp = SeatingProblem(DATA_DIR,subset=True, desired_num_of_people=10)

    # solving the problem using a selected solver, locally of using neos
    sp.solve(local=True, solvername='glpk')
    sp.print_res(versbose=False)

    #printing the output to the result file
    output_file = Path() / "out" / "res.txt"
    sp.m.display(filename=output_file)
