
"""
@author: NathanSankary

This script runs the Genetic algorithm for the proposed employee seating problem.
The parameters of the GA can be found below, and are set in the pb.ga_params() method.
Running this script prodces 2 files, 1. the solution saved as a csv, and 2. an interactive
graph plotting the solution as an .html file.

"""
import random
import numpy as np
import pandas as pd
import networkx as nx
import csv
from pathlib import Path
from bokeh.io import show, output_file
from bokeh.models import (
    Plot,
    Range1d,
    MultiLine,
    Circle,
    HoverTool,
    BoxZoomTool,
    ResetTool,
    StaticLayoutProvider,
    TapTool,
)
from bokeh.models.graphs import from_networkx, NodesAndLinkedEdges, EdgesAndLinkedNodes
from bokeh.palettes import brewer
from bokeh.plotting import figure





DATA_DIR = "comp_data"


class SeatingProblem_GA:
    def __init__(self, dir_name):
        # Build a number of dictionaries to use in the GA. Class attributes
        # are defined at the end of the initilizer.

        # Build a dictionary of people's names to numbers
        df_p = pd.read_csv(Path.cwd() / dir_name / "people_data.csv")
        df_p.dropna(axis=1, inplace=True)
        df_p.columns = ["id", "boss", "style"]

        counter = 1
        person_2_id = {}
        id_2_person = {}
        for person in df_p["id"]:
            person_2_id[person] = counter
            id_2_person[counter] = person
            counter += 1

        # Build a dictionary of the seats and employees that are fixed
        df_s = pd.read_csv(Path.cwd() / dir_name / "spaces_data.csv")
        df_s.columns = ["id", "function", "conc", "value", "current", "fixed"]
        # df_s_es=df_s.copy()
        df_s = df_s.loc[df_s["function"].isin(["Cubicle", "Office"])]
        df_s_all = df_s.copy()
        chrom_2_soln = {}
        chrom_counter = 0
        soln_counter = 0
        for i, val in df_s["fixed"].iteritems():
            if val == "yes":
                soln_counter += 1
            else:
                chrom_2_soln[chrom_counter] = soln_counter
                chrom_counter += 1
                soln_counter += 1

        # Build a dictionary of EVERY desk to an index
        counter = 0
        index_2_desk_all = {}
        desk_2_index_all = {}
        for i in range(len(df_s_all["id"])):
            index_2_desk_all[counter] = df_s_all["id"].iloc[i]
            desk_2_index_all[df_s_all["id"].iloc[i]] = counter
            counter += 1

        df_s_f = df_s.loc[df_s["fixed"] == "yes"]
        df_s = df_s.loc[df_s["fixed"] != "yes"]

        # Build a dictionary of the employes' who are in fixed locations
        fixed_employees = set()
        emp_2_fixed = {}
        for i in range(len(df_s_f["id"])):
            fixed_employees.add(person_2_id[df_s_f["current"].iloc[i]])
            emp_2_fixed[person_2_id[df_s_f["current"].iloc[i]]] = df_s_f["id"].iloc[i]

        # Build a dictionary of only the variable-seating employees
        id_2_person_var = id_2_person.copy()
        for emp in fixed_employees:
            del id_2_person_var[emp]

        # Build a dictionary of the VARIABLE seats to indecies
        counter = 0
        index_2_desk = {}
        concentrating_desk = set()
        desk_2_index = {}
        for i in range(len(df_s["id"])):
            index_2_desk[counter] = df_s["id"].iloc[i]
            desk_2_index[df_s["id"].iloc[i]] = counter
            if df_s["conc"].iloc[i] >= 3:
                concentrating_desk.add(counter)
            counter += 1

        # Build a dictionary of peoples' bosses
        emp_2_boss = {}
        for i in range(len(df_p["id"])):
            emp_2_boss[person_2_id[df_p["id"][i]]] = person_2_id[df_p["boss"][i]]

        # Building a set of concentrators
        counter = 1
        concentrators = set()
        for i in range(len(df_p["id"])):
            if "Concentrator" in df_p["style"][i]:
                concentrators.add(counter)
                counter += 1

        # Build  a dictionary of the space indecies of ALL (including non-feasible desks)
        df_s_all = pd.read_csv(Path.cwd() / dir_name / "spaces_data.csv")
        df_s_all.columns = ["id", "function", "conc", "value", "current", "fixed"]

        counter = 0
        space_2_index_all = {}
        index_2_space_all = {}
        desk_2_value = {}
        for i in range(len(df_s_all["id"])):
            space_2_index_all[df_s_all["id"].iloc[i]] = counter
            index_2_space_all[counter] = df_s_all["id"].iloc[i]
            desk_2_value[counter] = df_s_all["value"].iloc[i]
            counter += 1

        # Build the distance matrix
        df_d = pd.read_csv(Path.cwd() / dir_name / "dist.csv", sep=",", header=0)
        df_d.drop(df_d.columns[0], axis=1, inplace=True)
        distances = df_d.values

        # Build the collaboration Matrix
        df_c = pd.read_csv(Path.cwd() / dir_name / "coll_mat.csv", sep=",", header=0)
        df_c.fillna(
            9999, inplace=True
        )  # In the case of na collaboration assign a very high number (high penalty)
        df_c.drop(df_c.columns[0], axis=1, inplace=True)
        collaboration = df_c.values

        # Build a dictionary of the nidex of all var seats to ALL seats
        solution_2_seats = {}
        seats_2_solution = {}
        for i, val in index_2_desk_all.items():
            solution_2_seats[i] = space_2_index_all[val]
            seats_2_solution[space_2_index_all[val]] = i

        # Build a dictionary of executives to assistants
        df = pd.read_csv(Path.cwd() / dir_name / "assistants.csv")
        df.columns = ["assistant", "exec"]
        secretaries = {}
        for i in range(len(df["exec"])):
            secretaries[person_2_id[df["exec"].iloc[i]]] = person_2_id[
                df["assistant"].iloc[i]
            ]

        # Build a mapping of the number the "ranking" of each employee as a
        # directed graph.
        dg = nx.DiGraph()
        for emp, boss in emp_2_boss.items():
            dg.add_edge(boss, emp)

        emp_level = {}
        level_counter = 0
        while dg.nodes():
            emp_2_remove = []
            for node in list(dg.nodes()):
                if not nx.algorithms.dag.ancestors(dg, node):
                    emp_level[node] = level_counter
                    emp_2_remove.append(node)
            for node in emp_2_remove:
                dg.remove_node(node)
            level_counter += 1

        dg = nx.DiGraph()
        for emp, boss in emp_2_boss.items():
            dg.add_edge(boss, emp)

        # Define all attributes
        self.person_2_id = person_2_id
        self.id_2_person = id_2_person
        self.chrom_2_soln = chrom_2_soln
        self.index_2_desk_all = index_2_desk_all
        self.desk_2_index_all = desk_2_index_all
        self.emp_2_fixed = emp_2_fixed
        self.id_2_person_var = id_2_person_var
        self.concentrating_desk = concentrating_desk
        self.index_2_desk = index_2_desk
        self.desk_2_index = desk_2_index
        self.emp_2_boss = emp_2_boss
        self.concentrators = concentrators
        self.space_2_index_all = space_2_index_all
        self.index_2_space_all = index_2_space_all
        self.desk_2_value = desk_2_value
        self.distances = distances
        self.collaboration = collaboration
        self.solution_2_seats = solution_2_seats
        self.seats_2_solution = seats_2_solution
        self.secretaries = secretaries
        self.rank_graph = dg
        self.emp_level = emp_level
        self.gr_plot = None
        self.NGEN = None
        self.gen_count = 1

    def template_mapping(self, input_soln):
        # Convert the GA population chromosomes to standardized representatons
        # of solutions for evaluation.

        n_seats = len(self.solution_2_seats)
        arrangement = [0] * n_seats
        for emp_id, seat in self.emp_2_fixed.items():
            mapped_seat = self.desk_2_index_all[seat]
            arrangement[mapped_seat] = emp_id

        for seat, emp_id in enumerate(input_soln):
            mapped_seat = self.chrom_2_soln[seat]
            arrangement[mapped_seat] = emp_id

        output_soln = [0] * len(self.distances)
        for seat, emp_id in enumerate(arrangement):
            mapped_seat = self.solution_2_seats[seat]
            output_soln[mapped_seat] = emp_id
        return output_soln

    def template_output(self, input_soln):
        # Merge the GA solution to a final standardized output.

        n_seats = len(self.solution_2_seats)
        arrangement = [0] * n_seats
        out_dict = {}
        for emp_id, seat in self.emp_2_fixed.items():
            mapped_seat = self.desk_2_index_all[seat]
            arrangement[mapped_seat] = emp_id
        for seat, emp_id in enumerate(input_soln):
            mapped_seat = self.chrom_2_soln[seat]
            arrangement[mapped_seat] = emp_id
        output_soln = [0] * len(self.distances)
        for seat, emp_id in enumerate(arrangement):
            mapped_seat = self.solution_2_seats[seat]
            output_soln[mapped_seat] = emp_id
        for i, emp in enumerate(output_soln):
            if emp > 0:
                out_dict[self.index_2_space_all[i]] = self.id_2_person[emp]
        return out_dict

    def tournament(self, scores, players):
        # Perform a tournament to select the parents from the population.
        best = float("inf")
        for player in players:
            if scores[player] < best:
                winner = player
                best = scores[player]
        return winner

    def obj_fn(self, arrangement, report=False):
        # Compute a fitness (objective score) for each solution -- This is attempts
        # to minimize the distance between collaborators, while penalizing solutions
        # with employee rank -- seat attractiveness violations, secretary distance
        # violations, and concentrator seat violations, as defined in the problem
        # statement.

        # Duild a standardized representation of the solutuion.
        arrangement = self.template_mapping(arrangement)

        # Compute the number of people sitting in sesats with more attractiveness
        # than their superiors.
        ranking_violations = self.ranking_check(arrangement)

        # Compute the distance bettwen secretaries and their executive when NOT
        # sitting in the closest seat.
        sec_violations = self.secretary_check(arrangement)

        # Compute the number of concentrators not in concentrator desks.
        concentrater_violations = self.concentrater_counts(arrangement)

        # Compute the "collaborative distance" objective
        collab_dist = self.collab_distances(arrangement)
        wght = self.obj_weights

        # Sum all above values after multiplying by the respective objective weight.
        score = (
            ranking_violations * wght[0]
            + sec_violations * wght[1]
            + concentrater_violations * wght[2]
            + wght[3] * collab_dist
        )

        if report == True:
            print(
                "The number of ranking violations is: %d (fitness: %d)"
                % (ranking_violations, int(ranking_violations * wght[0]))
            )
            print(
                "The number of secretary violations is %d (fitness: %d)"
                % (sec_violations, int(sec_violations * wght[1]))
            )
            print(
                "The number of concentrator violations is %d (fitness: %d)"
                % (concentrater_violations, int(concentrater_violations * wght[2]))
            )
            print(
                "The total collaborative distance is: %d (fitness: %d)\n"
                % (int(collab_dist), int(collab_dist * wght[3]))
            )

        return score

    def obj_fn_constraints(self, arrangement, report=False):
        # Compute a fitness (objective score) for each solution's constrants
        # This is scores only the components of the objective defined as
        # CONSTRAINTS.

        # Duild a standardized representation of the solutuion.
        arrangement = self.template_mapping(arrangement)

        # Compute the number of people sitting in sesats with more attractiveness
        # than their superiors.
        ranking_violations = self.ranking_check(arrangement)

        # Compute the distance bettwen secretaries and their executive when NOT
        # sitting in the closest seat.
        sec_violations = self.secretary_check(arrangement)

        wght = self.obj_weights
        # Sum all above values after multiplying by the respective objective weight.
        score = (ranking_violations, sec_violations)

        if report == True:
            print(
                "The number of ranking violations is: %d (fitness: %d)"
                % (ranking_violations, int(ranking_violations * wght[0]))
            )
            print(
                "The number of secretary violations is %d (fitness: %d)"
                % (sec_violations, int(sec_violations * wght[1]))
            )

        return score

    def ranking_check(self, arrangement):
        # Return the number of times when an employee sits in a seat better
        # than their boss.
        seat_ranking = self.desk_2_value
        graph = self.rank_graph
        count = 0
        person_2_seat_tmp = {}
        for seat, person in enumerate(arrangement):
            person_2_seat_tmp[person] = seat

        for seat, person in enumerate(arrangement):
            if person > 0:
                seat_val = seat_ranking[seat]
                superiors = nx.algorithms.dag.ancestors(graph, person)
                for sup in superiors:
                    sup_seat = person_2_seat_tmp[sup]
                    sup_val = seat_ranking[sup_seat]
                    if sup_val < seat_val:
                        count += 1
        return count

    def secretary_check(self, arrangment):
        # Compute the distance beyond the nearest seat that an assigned
        # secretary sits in.

        secretaries = self.secretaries
        distances = self.distances
        distance = 0
        dist_data = distances.copy()
        for i in range(len(dist_data)):
            dist_data[i][i] = 999999
        for i, emp in enumerate(arrangment):
            if emp in secretaries:
                secretary = secretaries[emp]
                sec_desk = np.argmin(dist_data[i])
                if arrangment[sec_desk] == secretary:
                    distance += 0
                else:
                    sec_desk = arrangment.index(secretary)
                    distance += dist_data[i][sec_desk]
        return distance

    def concentrater_counts(self, arrangement):
        # Count the instances of concentrators not in concentrator seats.
        concentrators = self.concentrators
        conc_seats = self.concentrating_desk
        count = 0
        for emp, i in enumerate(arrangement):
            if (emp in concentrators) and (i not in conc_seats):
                count += 1
        return count

    def collab_distances(self, arrangement):
        # Cummulate the agreement of collaboration and distances between employees.
        # 2 employees with low collab. scores and small distances should score lowest.
        # 2 employees with high (bad) collab and large distances should score low.
        # 2 employees with low collab and high distance OR high collab (bad) and
        # small distance should be scored high.
        distances = self.distances
        collab_scores = self.collaboration

        max_dist = np.max(np.max(distances))
        med_collab = np.median(np.median(collab_scores))
        arr_dist = 0
        for seat, emp in enumerate(arrangement):
            if (
                emp not in self.secretaries.values()
            ):  # Ignore the secretaries except in cmputing their distances.
                if emp > 0:
                    emp_dist = 0
                    for seat_c, emp_c in enumerate(arrangement):
                        if (
                            seat_c != seat
                            and emp_c != emp
                            and collab_scores[emp - 1][emp_c - 1] < med_collab
                        ):
                            weight = (
                                collab_scores[emp - 1][emp_c - 1]
                            ) / med_collab  # small collab _scores are good
                            dist = distances[seat][seat_c] / max_dist
                            emp_dist += abs(weight - dist) + np.max([weight, dist])
                    arr_dist += emp_dist
        return arr_dist

    def initilize_solution(self, seats_available, employees):
        # Initilize random solutions that are permutations of employees at all seats.
        emp_set = set(employees.keys())
        n_seats = len(seats_available)
        soln = [0] * n_seats
        counter = 0
        neg_counter = -1
        while emp_set:
            soln[counter] = emp_set.pop()
            counter += 1
        random.shuffle(soln)
        for i in range(len(soln)):
            if soln[i] == 0:
                soln[i] = neg_counter
                neg_counter -= 1
        return soln

    def partially_matched_cx(self, mother, father):
        # Partially Mixed Crossover for a GA. A permutation based crossover operator.
        # Places a swath of the mother solution directly in to the child.
        # Merges the values present in father, not in mother, and in swath of father.
        # Fills any values after the swath and merge with the Father values.

        child = [0] * (len(mother))
        completed = [i for i in range(0, len(mother))]

        # Placing the swath from mother to child
        p1 = random.randint(0, len(mother) - 2)
        p2 = random.randint(p1 + 1, len(mother) - 1)
        for i in range(p1, p2):
            child[i] = mother[i]
            completed.remove(i)

        # Place the values in the swath of father that are not yet NOT in child
        for i in range(p1, p2):
            val_check = father[i]
            if val_check not in child:
                match = mother[i]
                father_match = father.index(match)
                while father_match in range(p1, p2):
                    match = mother[father_match]
                    father_match = father.index(match)
                child[father_match] = val_check
                completed.remove(father_match)

        # Filling final values from the father
        for i in completed:
            child[i] = father[i]
        return child

    def ga_params(self, NGEN, POP_SIZE, WEIGHTS, CROSSP, MUTP, N_PLAYERS):
        self.CROSSP = CROSSP  # The crossover probabilty.
        self.MUTP = MUTP  # The probabilty of mutation.
        self.N_PLAYERS = N_PLAYERS  # The size of the tournament.
        self.NGEN = NGEN  # The number of ga generations
        self.POP_SIZE = POP_SIZE  # The size of the GA population
        self.obj_weights = WEIGHTS  # The weights of the objective function.

    def run_ga(self):

        if self.NGEN is None:
            print(
                "The GA parameters have not been defined, try running YOUR-OBJECT-NAME.ga_params() with the required input first."
            )

        if self.gen_count == 1:
            # Initilize the GA
            self.mean_pop_fitness = (
                []
            )  # Unused but can be valuable for evaluating the GA
            self.min_pop_fitness = (
                []
            )  # Unused but can be valuable for evaluating the GA
            self.best_sol = float(
                "inf"
            )  # Initial best performance (for a minimization problem)

            # Generate an initial population
            population = []
            for i in range(self.POP_SIZE):
                temp = self.initilize_solution(
                    self.index_2_desk.keys(), self.id_2_person_var
                )
                population.append(temp)

            # Evaluate the entire population
            print("Evaluating the initial population")
            fitness = []
            c = 1
            for individual in population:
                score = self.obj_fn(individual.copy())
                if c % 50 == 0:
                    print(
                        "Evaluated %d percent of the population."
                        % int(c / len(population) * 100)
                    )
                fitness.append(score)
                c = c + 1
        else:
            population = self.current_pop
            fitness = self.current_fitness
        # Run NGENs (number of generations) of the GA
        print("Begining the evolutionary loops")

        for g in range(self.gen_count, self.NGEN):

            # Select the next pairs of parents
            parents = []
            for i in range(len(population)):
                t1 = set()
                t2 = set()
                while len(t1) < self.N_PLAYERS:
                    t1.add(random.randint(0, len(population) - 1))
                while len(t2) < self.N_PLAYERS:
                    t2.add(random.randint(0, len(population) - 1))
                p1 = self.tournament(fitness, t1)
                p2 = self.tournament(fitness, t2)
                parents.append([p1, p2])

            # Apply crossover on the offspring
            offspring = []
            for pair in parents:
                cross_check = random.random()
                if cross_check < self.CROSSP:
                    # crossover
                    child = self.partially_matched_cx(
                        population[pair[0]], population[pair[1]]
                    )
                    offspring.append(child)
                else:
                    offspring.append(population[pair[0]])

            # Apply mutation to offspring
            for i in range(len(offspring)):
                mut_check = random.random()
                if mut_check < self.MUTP:
                    # mutate
                    child = offspring[i]
                    mut_inds = set()
                    while len(mut_inds) < 2:
                        mut_inds.add(random.randint(0, len(child) - 1))
                    mut_inds = list(mut_inds)
                    v1 = child[mut_inds[0]]
                    child[mut_inds[0]] = child[mut_inds[1]]
                    child[mut_inds[1]] = v1

            # Elitesim operator -- places best solution of generation unmodified in to next generation
            if g > 1:
                offspring[0] = self.solution

            # Evaluate all individuals
            fitness = []
            c = 1
            for individual in offspring:
                score = self.obj_fn(individual)
                if c % 50 == 0:
                    print(
                        "Evaluated %d percent of the population."
                        % int(c / len(population) * 100)
                    )
                fitness.append(score)
                c += 1

            # Updating the best solution thus far
            if min(fitness) < self.best_sol:
                self.best_sol = min(fitness)
                self.solution = offspring[np.argmin(fitness)]
            print("\nCompleted Generation #%d\n" % g)
            print("\nBest objective so far is: %f\n" % self.best_sol)

            # Providing some info about the best solution
            unused = self.obj_fn(self.solution, report=True)

            # The population is replaced by the offspring
            population = offspring
            self.mean_pop_fitness.append(np.mean(fitness))
            self.min_pop_fitness.append(min(fitness))
            self.current_pop = population
            self.current_fitness = fitness

        # Write the best solution found with the GA to a dictionary
        self.gen_count = g
        seat_2_person = self.template_output(self.solution)
        self.best_fitness = self.best_sol
        person_2_seat = {}
        for seat, person in seat_2_person.items():
            person_2_seat[person] = seat
        self.seat_2_person = seat_2_person
        self.person_2_seat = person_2_seat

        return self.solution

    def secretary_swapping(self, output):
        output_sec = output.copy()
        distances = self.distances
        initial_violations = self.obj_fn_constraints(output, report=True)

        for boss, sec in self.secretaries.items():
            nearby = distances[boss]
            nearest = np.argsort(nearby)
            # boss_name=self.id_2_person[boss]
            sec_name = self.id_2_person[sec]
            seat_name = self.index_2_space_all[nearest[1]]  # Checking the nearest seat
            if self.person_2_seat[sec_name] != seat_name:
                # Enter a local search loop
                for ind in nearest:
                    if ind in self.index_2_desk and ind != output_sec.index(sec):
                        output_tm = output_sec.copy()
                        person_temp = output_tm[ind]
                        person_ind = output_tm.index(sec)
                        output_tm[ind] = sec
                        output_tm[person_ind] = person_temp
                        ranking_violations = self.obj_fn_constraints(
                            output_tm, report=False
                        )
                        # print(ranking_violations)
                        if (
                            ranking_violations[0] <= initial_violations[0]
                            and ranking_violations[1] < initial_violations[1]
                        ):  # This option swapps when and if the secretaries are closer and do not violate any seat ranking constraints.
                            # if ranking_violations[1]<initial_violations[1]: # This option swapps when and if the secretaries are closer
                            output_sec = output_tm
                            ranking_violations = self.obj_fn_constraints(
                                output_sec, report=True
                            )
                            print(
                                "There was a swap between seats %s, and %s."
                                % (
                                    self.index_2_space_all[ind],
                                    self.index_2_space_all[person_ind],
                                )
                            )
                            break
            else:
                print("Secretary %d is already closest to boss %d" % (sec, boss))

        print("\nThe effect of the swap is:")
        t = self.obj_fn(output, report=True)
        print("To:")
        t = self.obj_fn(output_sec, report=True)

        # Re-2rite the best found solutions so far.
        seat_2_person = self.template_output(output_sec)
        person_2_seat = {}
        for seat, person in seat_2_person.items():
            person_2_seat[person] = seat
        self.seat_2_person = seat_2_person
        self.person_2_seat = person_2_seat

        return (seat_2_person, person_2_seat)

    def seat_ranking_check(self):
        viol_count = 0
        for person, seat in self.person_2_seat.items():
            boss_prev = person
            seat_score = self.desk_2_value[self.space_2_index_all[seat]]
            boss = self.id_2_person[self.emp_2_boss[self.person_2_id[person]]]
            while boss != boss_prev:
                boss_seat_score = self.desk_2_value[
                    self.space_2_index_all[self.person_2_seat[boss]]
                ]
                if boss_seat_score < seat_score:
                    viol_count += 1
                    print(
                        "VIOLATION: Employee: %s of rank: %d can not sit in seat %s with attractiveness: %d \n with boss: %s of rank: %d in seat: %s of attractiveness: %d"
                        % (
                            person,
                            self.emp_level[self.person_2_id[person]],
                            seat,
                            self.desk_2_value[self.space_2_index_all[seat]],
                            boss,
                            self.emp_level[self.person_2_id[boss]],
                            self.person_2_seat[boss],
                            boss_seat_score,
                        )
                    )
                boss_prev = boss
                boss = boss = self.id_2_person[self.emp_2_boss[self.person_2_id[boss]]]

        print(
            "\nThere are %d rank-attractiveness violations in the arrangement.\n"
            % viol_count
        )
        return

    def secretary_violation_check(self):
        viol_count = 0
        for boss, secretary in self.secretaries.items():
            seat_ix = self.space_2_index_all[self.person_2_seat[self.id_2_person[boss]]]
            sec_seat = self.person_2_seat[self.id_2_person[secretary]]
            neighboors = self.distances[seat_ix]
            nearest = np.argsort(neighboors)
            nearest_seat = self.index_2_space_all[nearest[1]]
            if nearest_seat == sec_seat:
                print(
                    "Secretary %s is in the closest seat to employee %s"
                    % (self.id_2_person[secretary], self.id_2_person[boss])
                )
            else:
                sec_seat_ix = self.space_2_index_all[sec_seat]
                sec_seat_rank = np.where(nearest == sec_seat_ix)
                print(
                    "VIOLATION: Secretary %s is in the %d-closest seat to employee %s"
                    % (
                        self.id_2_person[secretary],
                        sec_seat_rank[0],
                        self.id_2_person[boss],
                    )
                )
                viol_count += 1
                # Explore why this happens:
        print(
            "\nThere are %d secretary volations out of %d secretaries."
            % (viol_count, len(self.secretaries))
        )

        return

    def save_solution(self):
        w = csv.writer(
            open("solution_output_GA.csv", "w")
        )
        for key, val in self.seat_2_person.items():
            w.writerow([key, val])

    def prepare_plot(self,dir_name):

        df_p = pd.read_csv(Path.cwd() / dir_name / "people_data.csv")
        df_p.dropna(axis=1, inplace=True)
        df_p.columns = ["id", "boss", "style"]
        # Build assorted dictionaries to assign values to graph nodes
        emp_2_style = dict(zip(df_p["id"], df_p["style"]))
        dsk_2_style = {}
        for emp, style in emp_2_style.items():
            dsk_2_style[self.person_2_seat[emp]] = style
        dsk_2_rnk = {}
        for emp, rnk in self.emp_level.items():
            emp_name = self.id_2_person[emp]
            dsk_2_rnk[self.person_2_seat[emp_name]] = rnk
        dsk_2_value = {}
        for seat, emp in self.seat_2_person.items():
            desk_index = self.space_2_index_all[seat]
            dsk_2_value[seat] = self.desk_2_value[desk_index]

        collab_thresh = (np.median(np.median(self.collaboration))) / 2
        cols = brewer["Accent"][6]
        styles = list(set(dsk_2_style.values()))
        styles.sort()
        style_2_color = {}
        for i, style in enumerate(styles):
            style_2_color[style] = cols[i]

        gr = nx.Graph()
        for desk, emp in self.seat_2_person.items():
            temp_ind = self.desk_2_index_all[desk]
            if temp_ind in self.concentrating_desk:
                concentrator = "True"
            else:
                concentrator = "False"
            gr.add_node(
                desk,
                color=style_2_color[dsk_2_style[desk]],
                concentrate=concentrator,
                desk_name=desk,
            )
        nx.set_node_attributes(gr, self.seat_2_person, "person")
        nx.set_node_attributes(gr, dsk_2_style, "style")
        nx.set_node_attributes(gr, dsk_2_value, "value")
        nx.set_node_attributes(gr, dsk_2_rnk, "rank")

        col_dist = brewer["RdYlGn"][11]
        # max_dist=np.max(np.max(self.distances))
        med_dist = np.median(np.median(self.distances))
        line_size = 5
        for index1, n1 in self.id_2_person.items():
            for index2, n2 in self.id_2_person.items():
                if self.collaboration[index1 - 1][index2 - 1] < collab_thresh:
                    link_trans = 1 - (
                        self.collaboration[index1 - 1][index2 - 1] / collab_thresh
                    )
                    # print("The width is: %f" % link_trans)
                    # print("The score is: %f" % collaboration[index1-1][index2-1])
                    dk1 = self.space_2_index_all[self.person_2_seat[n1]]
                    dk2 = self.space_2_index_all[self.person_2_seat[n2]]
                    dist_value = max(
                        0, 1 - self.distances[dk1][dk2] / (med_dist)
                    )  # modifying the med_dist changes the stricktness for plotting lines based on distance
                    conn = n1 + " to " + n2
                    color_ind = min(
                        10,
                        int(
                            (
                                self.collaboration[index1 - 1][index2 - 1]
                                / (collab_thresh)
                            )
                            * 10
                        ),
                    )
                    gr.add_edge(
                        self.person_2_seat[n1],
                        self.person_2_seat[n2],
                        transp=max(0.2, link_trans * 3),
                        edge_color=col_dist[color_ind],
                        collab=self.collaboration[index1 - 1][index2 - 1],
                        dist=max(0.05, dist_value),
                        size=dist_value * 3,
                        connection=conn,
                    )
                if index1 in self.secretaries:
                    if self.secretaries[index1] == index2:
                        dk1 = self.space_2_index_all[self.person_2_seat[n1]]
                        dk2 = self.space_2_index_all[self.person_2_seat[n2]]
                        dist_value = max(
                            0, 1 - self.distances[dk1][dk2] / (med_dist * 2)
                        )
                        conn = n1 + " to " + n2
                        gr.add_edge(
                            self.person_2_seat[n1],
                            self.person_2_seat[n2],
                            transp=2.5,
                            edge_color="#000000",
                            collab=self.collaboration[index1 - 1][index2 - 1],
                            dist=dist_value,
                            size=line_size,
                            connection=conn,
                        )
        self.gr_plot = gr
        df_s = pd.read_csv(Path.cwd() / dir_name / "spaces_data_coords.csv")
        df_s.columns = ["id", "conc", "value", "x", "y"]

        self.coordinates = {}
        for i in range(len(df_s)):
            self.coordinates[df_s["id"].iloc[i]] = [
                df_s["x"].iloc[i],
                df_s["y"].iloc[i],
            ]

        return

    def plot(self):
        # Check if the solution has been prepared to plot.
        if self.gr_plot is None:
            print(
                "The graph to plot has not been defined. Try running YOUR-OBJECT-NAME.prepare_plot() first."
            )
        else:
            TITLE = "Employee seating arrangement"
            plot = figure(
                plot_width=1000,
                plot_height=1000,
                x_range=Range1d(0, 370),
                y_range=Range1d(0, 250),
            )
            plot.title.text = TITLE

            node_hover_tool = HoverTool(
                tooltips=[
                    ("Employee:", "@person"),
                    ("Desk name:", "@desk_name"),
                    ("Employee rank:", "@rank"),
                    ("Employee style:", "@style"),
                    ("Seat value:", "@value"),
                    ("Concentrating desk:", "@concentrate"),
                ]
            )

            graph_renderer = from_networkx(
                self.gr_plot, nx.spring_layout, scale=0.75, center=(0, 0)
            )
            fixed_layout_provider = StaticLayoutProvider(graph_layout=self.coordinates)
            graph_renderer.layout_provider = fixed_layout_provider

            graph_renderer.node_renderer.glyph = Circle(size=10, fill_color="color")
            graph_renderer.node_renderer.selection_glyph = Circle(
                size=12, fill_color="color"
            )
            graph_renderer.edge_renderer.glyph = MultiLine(
                line_color="edge_color", line_alpha="dist", line_width="size"
            )
            graph_renderer.edge_renderer.selection_glyph = MultiLine(
                line_color="black", line_alpha=0.8, line_width=4
            )
            graph_renderer.selection_policy = NodesAndLinkedEdges()

            plot.renderers.append(graph_renderer)
            plot.add_tools(node_hover_tool, TapTool())

            output_file("Employee_seating_solution_graphic_GreedyAlgorithm.html")
            show(plot)
        return


if __name__ == "__main__":
    pb = SeatingProblem_GA(DATA_DIR)
    pb.ga_params(
        NGEN=500,
        POP_SIZE=300,
        WEIGHTS=[500, 5, 2, 0.1],
        CROSSP=0.8,
        MUTP=0.05,
        N_PLAYERS=3,
    )
    soln = pb.run_ga()
    pb.secretary_swapping(soln)
    pb.seat_ranking_check()
    pb.secretary_violation_check()
    pb.save_solution()
    pb.prepare_plot(DATA_DIR)
    pb.plot()
