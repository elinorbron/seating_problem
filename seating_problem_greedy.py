

"""
@author: NathanSankary

This script runs the Greedy algorithm for the proposed employee seating problem.
The parameters of the algorithm can be found below, and are set in the pb.params() method.
Running this script prodces 2 files, 1. the solution saved as a csv, and 2. an interactive
graph plotting the solution as an .html file.

"""
import csv
import random
from pathlib import Path

import networkx as nx
import numpy as np
import pandas as pd
from bokeh.io import output_file, show
from bokeh.models import (
    BoxZoomTool,
    Circle,
    HoverTool,
    MultiLine,
    Plot,
    Range1d,
    ResetTool,
    StaticLayoutProvider,
    TapTool,
)
from bokeh.models.graphs import EdgesAndLinkedNodes, NodesAndLinkedEdges, from_networkx
from bokeh.palettes import brewer
from bokeh.plotting import figure


DATA_DIR = "comp_data"


class SeatingProblem_Greedy:
    def __init__(self, dir_name):
        # Build a number of dictionaries to use in the GA. Class attributes
        # are defined at the end of the initilizer.

        # Build a dictionry of people's names to numbers
        df_p = pd.read_csv(Path.cwd() / dir_name / "people_data.csv")
        df_p.dropna(axis=1, inplace=True)
        df_p.columns = ["id", "boss", "style"]

        counter = 1
        person_2_id = {}
        id_2_person = {}
        for person in df_p["id"]:
            person_2_id[person] = counter
            id_2_person[counter] = person
            counter += 1

        # Build a dictionary of the seats and employees that are fixed
        df_s = pd.read_csv(Path.cwd() / dir_name / "spaces_data.csv")
        df_s.columns = ["id", "function", "conc", "value", "current", "fixed"]
        # df_s_es=df_s.copy()
        df_s = df_s.loc[df_s["function"].isin(["Cubicle", "Office"])]
        df_s_all = df_s.copy()
        chrom_2_soln = {}
        chrom_counter = 0
        soln_counter = 0
        for i, val in df_s["fixed"].iteritems():
            if val == "yes":
                soln_counter += 1
            else:
                chrom_2_soln[chrom_counter] = soln_counter
                chrom_counter += 1
                soln_counter += 1

        # Build a dictionary of EVERY desk to an index
        counter = 0
        index_2_desk_all = {}
        desk_2_index_all = {}
        for i in range(len(df_s_all["id"])):
            index_2_desk_all[counter] = df_s_all["id"].iloc[i]
            desk_2_index_all[df_s_all["id"].iloc[i]] = counter
            counter += 1

        df_s_f = df_s.loc[df_s["fixed"] == "yes"]
        df_s = df_s.loc[df_s["fixed"] != "yes"]

        # Build a dictionary of the employes' who are in fixed locations
        fixed_employees = set()
        emp_2_fixed = {}
        for i in range(len(df_s_f["id"])):
            fixed_employees.add(person_2_id[df_s_f["current"].iloc[i]])
            emp_2_fixed[person_2_id[df_s_f["current"].iloc[i]]] = df_s_f["id"].iloc[i]

        # Build a dictionary of only the variable-seating employees
        id_2_person_var = id_2_person.copy()
        for emp in fixed_employees:
            del id_2_person_var[emp]

        # Build a dictionary of the VARIABLE seats to indecies
        counter = 0
        index_2_desk = {}
        concentrating_desk = set()
        desk_2_index = {}
        for i in range(len(df_s["id"])):
            index_2_desk[counter] = df_s["id"].iloc[i]
            desk_2_index[df_s["id"].iloc[i]] = counter
            if df_s["conc"].iloc[i] >= 3:
                concentrating_desk.add(counter)
            counter += 1

        # Build a dictionary of peoples' bosses
        emp_2_boss = {}
        boss_2_emp = {}
        for i in range(len(df_p["id"])):
            emp_2_boss[person_2_id[df_p["id"][i]]] = person_2_id[df_p["boss"][i]]
            if person_2_id[df_p["boss"][i]] not in boss_2_emp:
                boss_2_emp[person_2_id[df_p["boss"][i]]] = [person_2_id[df_p["id"][i]]]
            else:
                boss_2_emp[person_2_id[df_p["boss"][i]]].append(
                    person_2_id[df_p["id"][i]]
                )

        # Building a set of concentrators
        counter = 1
        concentrators = set()
        for i in range(len(df_p["id"])):
            if "Concentrator" in df_p["style"][i]:
                concentrators.add(counter)
                counter += 1

        # Build  a dictionary of the space indecies of ALL (including non-feasible desks)
        df_s_all = pd.read_csv(Path.cwd() / dir_name / "spaces_data.csv")
        df_s_all.columns = ["id", "function", "conc", "value", "current", "fixed"]

        counter = 0
        space_2_index_all = {}
        index_2_space_all = {}
        desk_2_value = {}
        for i in range(len(df_s_all["id"])):
            space_2_index_all[df_s_all["id"].iloc[i]] = counter
            index_2_space_all[counter] = df_s_all["id"].iloc[i]
            desk_2_value[counter] = df_s_all["value"].iloc[i]
            counter += 1

        # Build the distance matrix
        df_d = pd.read_csv(Path.cwd() / dir_name / "dist.csv", sep=",", header=0)
        df_d.drop(df_d.columns[0], axis=1, inplace=True)
        distances = df_d.values

        # Build the collaboration Matrix
        df_c = pd.read_csv(Path.cwd() / dir_name / "coll_mat.csv", sep=",", header=0)
        df_c.fillna(
            9999, inplace=True
        )  # In the case of na collaboration assign a very high number (high penalty)
        df_c.drop(df_c.columns[0], axis=1, inplace=True)
        collaboration = df_c.values

        # Build a dictionary of the nidex of all var seats to ALL seats
        solution_2_seats = {}
        seats_2_solution = {}
        for i, val in index_2_desk_all.items():
            solution_2_seats[i] = space_2_index_all[val]
            seats_2_solution[space_2_index_all[val]] = i

        # Build a dictionary of executives to assistants
        df = pd.read_csv(Path.cwd() / dir_name / "assistants.csv")
        df.columns = ["assistant", "exec"]
        secretaries = {}
        for i in range(len(df["exec"])):
            secretaries[person_2_id[df["exec"].iloc[i]]] = person_2_id[
                df["assistant"].iloc[i]
            ]

        # Build a mapping of the number the "ranking" of each employee as a
        # directed graph.
        dg = nx.DiGraph()
        for emp, boss in emp_2_boss.items():
            dg.add_edge(boss, emp)

        emp_level = {}
        level_counter = 0
        while dg.nodes():
            emp_2_remove = []
            for node in list(dg.nodes()):
                if not nx.algorithms.dag.ancestors(dg, node):
                    emp_level[node] = level_counter
                    emp_2_remove.append(node)
            for node in emp_2_remove:
                dg.remove_node(node)
            level_counter += 1

        dg = nx.DiGraph()
        for emp, boss in emp_2_boss.items():
            dg.add_edge(boss, emp)

        # Build a dictionary of emps in a "level"
        level_2_emp = {}
        for emp, level in emp_level.items():
            if level not in level_2_emp:
                level_2_emp[level] = [emp]
            else:
                level_2_emp[level].append(emp)

        # Build a dictionary of seats in a "value/attractiveness"
        value_2_seats = {}
        for seat, value in desk_2_value.items():
            if value not in value_2_seats:
                value_2_seats[value] = [seat]
            else:
                value_2_seats[value].append(seat)

        # Build a dictionary of the MAX level of workers below a worker
        dag = nx.DiGraph()
        emp_2_sublevels = {}
        for emp, boss in emp_2_boss.items():
            dag.add_node(emp)
            if emp != boss:
                dag.add_edge(boss, emp)

        order = list(nx.topological_sort(dag))

        for i in range(len(order)):
            current_emp = order[i]
            temp_nodes = order[i:]
            dag_temp = nx.DiGraph()
            sublevels = 0
            for node in temp_nodes:
                if node in boss_2_emp:
                    emps = boss_2_emp[node]
                    for emp in emps:
                        if emp != node:
                            dag_temp.add_edge(node, emp)
            sublevels = nx.dag_longest_path_length(dag_temp)
            emp_2_sublevels[current_emp] = sublevels

        # Define all attributes
        self.person_2_id = person_2_id
        self.id_2_person = id_2_person
        self.chrom_2_soln = chrom_2_soln
        self.index_2_desk_all = index_2_desk_all
        self.desk_2_index_all = desk_2_index_all
        self.emp_2_fixed = emp_2_fixed
        self.id_2_person_var = id_2_person_var
        self.concentrating_desk = concentrating_desk
        self.index_2_desk = index_2_desk
        self.desk_2_index = desk_2_index
        self.emp_2_boss = emp_2_boss
        self.concentrators = concentrators
        self.space_2_index_all = space_2_index_all
        self.index_2_space_all = index_2_space_all
        self.desk_2_value = desk_2_value
        self.distances = distances
        self.collaboration = collaboration
        self.solution_2_seats = solution_2_seats
        self.seats_2_solution = seats_2_solution
        self.secretaries = secretaries
        self.rank_graph = dg
        self.emp_level = emp_level
        self.gr_plot = None
        self.NGEN = None
        self.gen_count = 1
        self.level_2_emp = level_2_emp
        self.value_2_seats = value_2_seats
        self.emp_2_sublevels = emp_2_sublevels
        self.error_point = []

    def template_mapping(self, input_soln):
        # Convert the GA population chromosomes to standardized representatons
        # of solutions for evaluation.

        output_soln = [0] * len(self.distances)
        for seat, emp_id in enumerate(input_soln):
            mapped_seat = self.solution_2_seats[seat]
            output_soln[mapped_seat] = emp_id
        return output_soln

    def template_output(self, input_soln):
        # Merge the GA solution to a final standardized output.

        # output_soln=[-2]*len(self.distances)
        # for seat,emp_id in enumerate(input_soln):
        #    mapped_seat=self.solution_2_seats[seat]
        #    output_soln[mapped_seat]=emp_id
        out_dict = {}
        for i, emp in enumerate(input_soln):
            if emp > 0:
                out_dict[self.index_2_space_all[i]] = self.id_2_person[emp]
        return out_dict

    def obj_fn(self, arrangement, just_placed, report=False):
        # Compute a fitness (objective score) for each solution -- This is attempts
        # to minimize the distance between collaborators, while penalizing solutions
        # with employee rank -- seat attractiveness violations, secretary distance
        # violations, and concentrator seat violations, as defined in the problem
        # statement.

        # Duild a standardized representation of the solutuion.
        # arrangement=self.template_mapping(arrangement)

        # Compute the number of people sitting in sesats with more attractiveness
        # than their superiors.
        ranking_violations = 0
        # ranking_violations=self.ranking_check(arrangement)

        # Compute the distance bettwen secretaries and their executive when NOT
        # sitting in the closest seat.
        # sec_violations = 0
        sec_violations = self.secretary_check(arrangement)

        # Compute the number of concentrators not in concentrator desks.
        concentrater_violations = self.concentrater_counts(arrangement)

        # Compute the "collaborative distance" objective
        collab_dist = self.collab_distances(arrangement, just_placed)
        wght = self.WEIGHTS

        # Sum all above values after multiplying by the respective objective weight.
        score = (
            ranking_violations * wght[0]
            + sec_violations * wght[1]
            + concentrater_violations * wght[2]
            + wght[3] * collab_dist
        )

        if report == True:
            print(
                "The number of ranking violations is: %d (fitness: %d)"
                % (ranking_violations, int(ranking_violations * wght[0]))
            )
            print(
                "The number of secretary violations is %d (fitness: %d)"
                % (sec_violations, int(sec_violations * wght[1]))
            )
            print(
                "The number of concentrator violations is %d (fitness: %d)"
                % (concentrater_violations, int(concentrater_violations * wght[2]))
            )
            print(
                "The total collaborative distance is: %d (fitness: %d)\n"
                % (int(collab_dist), int(collab_dist * wght[3]))
            )

        return score

    def obj_fn_output(self, arrangement, report=False, sec_fix=False):
        # Compute a fitness (objective score) for each solution's constrants
        # This is scores only the components of the objective defined as
        # CONSTRAINTS.

        # Duild a standardized representation of the solutuion.
        # arrangement=self.template_mapping(arrangement)

        # Compute the number of people sitting in sesats with more attractiveness
        # than their superiors.
        ranking_violations = self.ranking_check(arrangement)

        # Compute the distance bettwen secretaries and their executive when NOT
        # sitting in the closest seat.
        sec_violations = self.secretary_check(arrangement)

        # Compute the number of concentrators not in concentrator desks.
        concentrater_violations = self.concentrater_counts(arrangement)

        # Compute the "collaborative distance" objective
        collab_dist = self.collab_distances_output(arrangement)

        # wght=self.WEIGHTS
        wght = [500, 5, 10, 0.1]  # Original GA weights
        # Sum all above values after multiplying by the respective objective weight.
        score = (
            ranking_violations * wght[0]
            + sec_violations * wght[1]
            + concentrater_violations * wght[2]
            + wght[3] * collab_dist
        )

        if report == True:
            print(
                "The number of ranking violations is: %d (fitness: %d)"
                % (ranking_violations, int(ranking_violations * wght[0]))
            )
            print(
                "The number of secretary violations is %d (fitness: %d)"
                % (sec_violations, int(sec_violations * wght[1]))
            )
            print(
                "The number of concentrator violations is %d (fitness: %d)"
                % (concentrater_violations, int(concentrater_violations * wght[2]))
            )
            print(
                "The total collaborative distance is: %d (fitness: %d)\n"
                % (int(collab_dist), int(collab_dist * wght[3]))
            )
            print("The total fitness score is: %d" % int(score))

        if sec_fix == False:
            return score
        else:
            return (ranking_violations, sec_violations)

    def ranking_check(self, arrangement):
        # Return the number of times when an employee sits in a seat better
        # than their boss.
        seat_ranking = self.desk_2_value
        graph = self.rank_graph
        count = 0
        person_2_seat_tmp = {}
        for seat, person in enumerate(arrangement):
            person_2_seat_tmp[person] = seat

        for seat, person in enumerate(arrangement):
            if person > 0:
                seat_val = seat_ranking[seat]
                superiors = nx.algorithms.dag.ancestors(graph, person)
                for sup in superiors:
                    sup_seat = person_2_seat_tmp[sup]
                    sup_val = seat_ranking[sup_seat]
                    if sup_val < seat_val:
                        count += 1
        return count

    def secretary_check(self, arrangment):
        # Compute the distance beyond the nearest seat that an assigned
        # secretary sits in.

        secretaries = self.secretaries
        distances = self.distances
        distance = 0
        dist_data = distances.copy()
        for i in range(len(dist_data)):
            dist_data[i][i] = 999999
        for i, emp in enumerate(arrangment):
            if emp in secretaries:
                secretary = secretaries[emp]
                neighboors = list(dist_data[i].copy())
                nearest = np.argmin(neighboors)
                while np.isnan(self.desk_2_value[nearest]) == True:
                    del neighboors[nearest]
                    nearest = np.argmin(neighboors)
                sec_desk = nearest
                if arrangment[sec_desk] == secretary:
                    distance += 0
                else:
                    sec_desk = arrangment.index(secretary)
                    distance += dist_data[i][sec_desk]
        return distance

    def concentrater_counts(self, arrangement):
        # Count the instances of concentrators not in concentrator seats.
        concentrators = self.concentrators
        conc_seats = self.concentrating_desk
        count = 0
        for emp, i in enumerate(arrangement):
            if (emp in concentrators) and (i not in conc_seats):
                count += 1
        return count

    def collab_distances(self, arrangement, just_placed):
        # Cummulate the agreement of collaboration and distances between employees.
        # 2 employees with low collab. scores and small distances should score lowest.
        # 2 employees with high (bad) collab and large distances should score low.
        # 2 employees with low collab and high distance OR high collab (bad) and
        # small distance should be scored high.
        distances = self.distances
        collab_scores = self.collaboration

        max_dist = np.max(np.max(distances))
        med_collab = np.median(np.median(collab_scores))
        arr_dist = 0
        comparisons = 1
        # for seat,emp in enumerate(arrangement):
        emp = just_placed[0]
        seat = just_placed[1]
        if emp not in self.secretaries.values():
            # Ignore the secretaries except in computing their distances.
            if emp > 0:
                comparisons += 1
                emp_dist = 0
                emp_comparisons = 1
                for seat_c, emp_c in enumerate(arrangement):
                    if (
                        seat_c != seat
                        and emp_c != emp
                        and collab_scores[emp - 1][emp_c - 1] < med_collab
                        and emp_c > 0
                    ):
                        emp_comparisons += 1
                        weight = (collab_scores[emp - 1][emp_c - 1]) / med_collab
                        dist = distances[seat][seat_c] / max_dist
                        emp_dist += abs(weight - dist) + np.max([weight, dist])
                arr_dist += emp_dist / emp_comparisons
        return arr_dist / comparisons

    def collab_distances_output(self, arrangement):
        # Cummulate the agreement of collaboration and distances between employees.
        # 2 employees with low collab. scores and small distances should score lowest.
        # 2 employees with high (bad) collab and large distances should score low.
        # 2 employees with low collab and high distance OR high collab (bad) and
        # small distance should be scored high.
        distances = self.distances
        collab_scores = self.collaboration

        max_dist = np.max(np.max(distances))
        med_collab = np.median(np.median(collab_scores))
        arr_dist = 0
        for seat, emp in enumerate(arrangement):
            if emp not in self.secretaries.values():
                # Ignore the secretaries except in cmputing their distances.
                if emp > 0:
                    emp_dist = 0
                    for seat_c, emp_c in enumerate(arrangement):
                        if (
                            seat_c != seat
                            and emp_c != emp
                            and collab_scores[emp - 1][emp_c - 1] < med_collab
                        ):
                            weight = (
                                collab_scores[emp - 1][emp_c - 1]
                            ) / med_collab  # small collab _scores are good
                            dist = distances[seat][seat_c] / max_dist
                            emp_dist += abs(weight - dist) + np.max([weight, dist])
                    arr_dist += emp_dist
        return arr_dist

    def secretary_placement(self, sol, secretaries, just_placed, interm_check=False):
        # just_placed is a tupe of the most recently placed employee ans seat index.

        emp_id = just_placed[0]
        seat_id = just_placed[1]
        if emp_id in secretaries:
            sec_id = secretaries[emp_id]
            nearby = self.distances[seat_id]
            nearest = np.argsort(nearby)
            nearest = nearest[1:]
            for place in nearest:
                if sol[place] < 0 and np.isnan(self.desk_2_value[place]) == False:
                    sol[place] = sec_id
                    if interm_check == False:
                        self.emp_2_place.remove(sec_id)
                    return sol
            # print("There were no available seats to place the secretary.")
        return sol

    def greedy_params(self, N_ITT=50, WEIGHTS=[0, 0, 10, 0.1]):
        self.N_ITT = N_ITT  # The crossover probabilty.
        self.WEIGHTS = WEIGHTS  # The size of the tournament.

    def run_greedy(self):

        if self.N_ITT is None:
            print(
                "The GREEDY parameters have not been defined, try running YOUR-OBJECT-NAME.greedy_params() with the required input first."
            )

        # Initilize greedy algorithm
        self.best_sol = float("inf")

        # Generate an initial solution
        sol = [-2] * len(self.distances)

        # Generate a set of employees to place
        self.emp_2_place = set(self.id_2_person.keys())

        # Place any required seating arrangements
        for emp_id, seat in self.emp_2_fixed.items():
            seat_id = self.space_2_index_all[seat]
            sol[seat_id] = emp_id
            self.emp_2_place.remove(emp_id)
            sol = self.secretary_placement(sol, self.secretaries, [emp_id, seat_id])
            print("Placed employee id: %d" % emp_id)
        # Should perform some sort of local search here

        print("Begining the greedy placement")
        # Build a list of emp ranks
        rankings = [0, 1, 2, 3, 4]  # Knew this before hand, should be more general

        for rank in rankings:
            employee_set = self.level_2_emp[rank]
            order_calm = []
            order_urgent = []
            for current_emp in employee_set:
                # Determine the order to place the next employees
                if current_emp in self.emp_2_place and current_emp not in set(
                    self.secretaries.values()
                ):
                    boss_temp = self.emp_2_boss[current_emp]
                    boss_seat = sol.index(boss_temp)
                    boss_seat_value = self.desk_2_value[boss_seat]
                    max_value = int(boss_seat_value)
                    min_value = self.emp_2_sublevels[current_emp]
                    available_values = range(min_value, max_value + 1)
                    if max_value == min_value:
                        order_urgent.append(current_emp)
                    else:
                        order_calm.append(current_emp)
            order = order_urgent + order_calm

            for person in order:
                boss_temp = self.emp_2_boss[person]
                boss_seat = sol.index(boss_temp)
                boss_seat_value = self.desk_2_value[boss_seat]
                max_value = int(boss_seat_value)
                min_value = self.emp_2_sublevels[person]
                # min_value=0
                available_values = range(min_value, max_value + 1)
                placement_values = [float("inf")] * len(sol)
                entered_search = 0
                for i in range(len(sol)):
                    sol_temp = sol.copy()
                    if sol_temp[i] < 0:
                        if (
                            self.desk_2_value[i] in available_values
                            and np.isnan(self.desk_2_value[i]) != True
                        ):
                            entered_search = 1
                            # Flag to determine if there was an available desk for the soln
                            sol_temp[i] = person
                            sol_temp = self.secretary_placement(
                                sol_temp,
                                self.secretaries,
                                [person, i],
                                interm_check=True,
                            )
                            placement_values[i] = self.obj_fn(sol_temp, [person, i])
                c = 0
                while entered_search == 0 and c < 6:
                    # Write a short log about the employee that could not be placed.
                    # Try reducing the stringency on the required min seat value
                    self.error_point.append([rank, person])
                    self.emp_2_sublevels[person] -= 1
                    min_value = self.emp_2_sublevels[person]
                    print(
                        "Needed to reduce the seat level for employee id: %d" % person
                    )
                    # min_value=0
                    available_values = range(min_value, max_value + 1)
                    placement_values = [float("inf")] * len(sol)
                    entered_search = 0

                    for i in range(len(sol)):
                        sol_temp = sol.copy()
                        if sol_temp[i] < 0:
                            if (
                                self.desk_2_value[i] in available_values
                                and np.isnan(self.desk_2_value[i]) != True
                            ):
                                entered_search = 1
                                # Flag to determine if there was an available desk for the soln
                                sol_temp[i] = person
                                sol_temp = self.secretary_placement(
                                    sol_temp,
                                    self.secretaries,
                                    [person, i],
                                    interm_check=True,
                                )
                                placement_values[i] = self.obj_fn(sol_temp, [person, i])
                    c += 1

                assigned_seat = np.argmin(placement_values)
                sol[assigned_seat] = person
                sol = self.secretary_placement(
                    sol, self.secretaries, [person, assigned_seat], interm_check=False
                )
                self.emp_2_place.remove(person)
                print("Placed employee id: %d" % person)

        # Write the best solution found with the greedy algorithm to a dictionary
        seat_2_person = self.template_output(sol)
        person_2_seat = {}
        for seat, person in seat_2_person.items():
            person_2_seat[person] = seat
        self.seat_2_person = seat_2_person
        self.person_2_seat = person_2_seat

        return sol

    def local_search(self, sol):
        # A simple Local search which accepts only emp swaps that do not
        # violate constraints and improve the distance.

        total_swaps = self.N_ITT
        swap_count = 0
        emp_candidates = list(self.id_2_person_var.keys())

        # Generate a dictionary of the employees in a seat rank
        rank_2_desk = {}
        for emp, seat in self.person_2_seat.items():
            seat_id = self.space_2_index_all[seat]
            seat_level = self.desk_2_value[seat_id]
            if seat_level in rank_2_desk:
                rank_2_desk[seat_level].append(seat_id)
            else:
                rank_2_desk[seat_level] = [seat_id]

        initial_value = self.obj_fn_output(sol, report=False)

        comp_value = initial_value
        while swap_count < total_swaps:
            if swap_count % 40 == 0:
                print("This is count number: %d" % swap_count)
            sol_temp = sol.copy()
            emp = emp_candidates[random.randint(0, len(emp_candidates)) - 1]
            emp_seat = sol_temp.index(emp)
            emp_seat_rank = self.desk_2_value[emp_seat]
            viable_seats = rank_2_desk[emp_seat_rank]
            swap_seat = viable_seats[random.randint(0, len(viable_seats) - 1)]
            swap_emp = sol_temp[swap_seat]
            sol_temp[emp_seat] = swap_emp
            sol_temp[swap_seat] = emp
            temp_value = self.obj_fn_output(sol_temp, report=False)
            # print('The temp_value is: %d, the initial value is: %d'% (temp_value,inital_value))
            if temp_value < comp_value:
                print(
                    "LS swap reduced the objective function by: %d"
                    % int(comp_value - temp_value)
                )
                comp_value = temp_value
                sol = sol_temp
            swap_count += 1

        seat_2_person = self.template_output(sol)
        person_2_seat = {}
        for seat, person in seat_2_person.items():
            person_2_seat[person] = seat
        self.seat_2_person = seat_2_person
        self.person_2_seat = person_2_seat

        return sol

    def secretary_swapping(self, output):
        output_sec = output.copy()
        distances = self.distances
        rank_viols, sec_viols = self.obj_fn_output(output, report=True, sec_fix=True)

        for boss, sec in self.secretaries.items():
            nearby = distances[boss]
            nearest = np.argsort(nearby)
            sec_name = self.id_2_person[sec]
            seat_name = self.index_2_space_all[nearest[1]]  # Checking the nearest seat
            if self.person_2_seat[sec_name] != seat_name:
                # Enter a local search loop
                for ind in nearest:
                    if ind in self.index_2_desk and ind != output_sec.index(sec):
                        output_tm = output_sec.copy()
                        person_temp = output_tm[ind]
                        person_ind = output_tm.index(sec)
                        output_tm[ind] = sec
                        output_tm[person_ind] = person_temp
                        temp_rank_viols, temp_sec_viols = self.obj_fn_output(
                            output_tm, report=False, sec_fix=True
                        )
                        # This option swapps when and if the secretaries are closer and do not violate any seat ranking constraints.
                        if (
                            temp_rank_viols <= rank_viols and temp_sec_viols < sec_viols
                        ):  # This option swapps when and if the secretaries are closer and do not violate any seat ranking constraints.
                            # This option swapps when and if the secretaries are closer
                            # if ranking_violations[1]<initial_violations[1]: # This option swapps when and if the secretaries are closer
                            output_sec = output_tm
                            rank_viols, sec_viols = self.obj_fn_output(
                                output_sec, report=True, sec_fix=True
                            )
                            print(
                                "There was a swap between seats %s, and %s."
                                % (
                                    self.index_2_space_all[ind],
                                    self.index_2_space_all[person_ind],
                                )
                            )
                            break
            # else:
            # print('Secretary %d is already closest to boss %d' % (sec,boss))

        print("\nThe effect of the swap is:")
        t = self.obj_fn_output(output, report=True)
        print("To:")
        t = self.obj_fn_output(output_sec, report=True)

        # Re-2rite the best found solutions so far.
        seat_2_person = self.template_output(output_sec)
        person_2_seat = {}
        for seat, person in seat_2_person.items():
            person_2_seat[person] = seat
        self.seat_2_person = seat_2_person
        self.person_2_seat = person_2_seat

        return output_sec

    def seat_ranking_check(self):
        viol_count = 0
        for person, seat in self.person_2_seat.items():
            boss_prev = person
            seat_score = self.desk_2_value[self.space_2_index_all[seat]]
            boss = self.id_2_person[self.emp_2_boss[self.person_2_id[person]]]
            while boss != boss_prev:
                boss_seat_score = self.desk_2_value[
                    self.space_2_index_all[self.person_2_seat[boss]]
                ]
                if boss_seat_score < seat_score:
                    viol_count += 1
                    print(
                        "VIOLATION: Employee: %s of rank: %d can not sit in seat %s with attractiveness: %d \n with boss: %s of rank: %d in seat: %s of attractiveness: %d"
                        % (
                            person,
                            self.emp_level[self.person_2_id[person]],
                            seat,
                            self.desk_2_value[self.space_2_index_all[seat]],
                            boss,
                            self.emp_level[self.person_2_id[boss]],
                            self.person_2_seat[boss],
                            boss_seat_score,
                        )
                    )
                boss_prev = boss
                boss = boss = self.id_2_person[self.emp_2_boss[self.person_2_id[boss]]]

        print(
            "\nThere are %d rank-attractiveness violations in the arrangement.\n"
            % viol_count
        )
        return

    def secretary_violation_check(self):
        viol_count = 0
        for boss, secretary in self.secretaries.items():
            seat_ix = self.space_2_index_all[self.person_2_seat[self.id_2_person[boss]]]
            sec_seat = self.person_2_seat[self.id_2_person[secretary]]
            neighboors = self.distances[seat_ix]
            nearest = np.argsort(neighboors)
            # for i in range(len(nearest)):
            #     if
            places = 1
            nearest_seat = self.index_2_space_all[nearest[places]]
            check = np.isnan(self.desk_2_value[nearest[places]])
            while check is True:
                places += 1
                check = np.isnan(self.desk_2_value[nearest[places]])
            nearest_seat = self.index_2_space_all[nearest[places]]
            if nearest_seat == sec_seat:
                t = 1
                # print('Secretary %s is in the closest seat to employee %s'% (self.id_2_person[secretary],self.id_2_person[boss]))
            else:
                sec_seat_ix = self.space_2_index_all[sec_seat]
                sec_seat_rank = np.where(np.array(nearest) == sec_seat_ix)
                print(
                    "VIOLATION: Secretary %s is in the %d-closest available seat to employee %s"
                    % (
                        self.id_2_person[secretary],
                        sec_seat_rank[0],
                        self.id_2_person[boss],
                    )
                )
                viol_count += 1
                # Explore why this happens:
        print(
            "\nThere are %d secretary volations out of %d secretaries."
            % (viol_count, len(self.secretaries))
        )
        return

    def save_solution(self):
        w = csv.writer(open("solution_output_Greedy.csv", "w"))
        for key, val in self.seat_2_person.items():
            w.writerow([key, val])

    def prepare_plot(self, dir_name):

        df_p = pd.read_csv(Path.cwd() / dir_name / "people_data.csv")
        df_p.dropna(axis=1, inplace=True)
        df_p.columns = ["id", "boss", "style"]
        # Build assorted dictionaries to assign values to graph nodes
        emp_2_style = dict(zip(df_p["id"], df_p["style"]))
        dsk_2_style = {}
        for emp, style in emp_2_style.items():
            dsk_2_style[self.person_2_seat[emp]] = style
        dsk_2_rnk = {}
        for emp, rnk in self.emp_level.items():
            emp_name = self.id_2_person[emp]
            dsk_2_rnk[self.person_2_seat[emp_name]] = rnk
        dsk_2_value = {}
        for seat, emp in self.seat_2_person.items():
            desk_index = self.space_2_index_all[seat]
            dsk_2_value[seat] = self.desk_2_value[desk_index]

        collab_thresh = (np.median(np.median(self.collaboration))) / 2
        cols = brewer["Accent"][6]
        styles = list(set(dsk_2_style.values()))
        styles.sort()
        style_2_color = {}
        for i, style in enumerate(styles):
            style_2_color[style] = cols[i]

        gr = nx.Graph()
        for desk, emp in self.seat_2_person.items():
            temp_ind = self.desk_2_index_all[desk]
            if temp_ind in self.concentrating_desk:
                concentrator = "True"
            else:
                concentrator = "False"
            gr.add_node(
                desk,
                color=style_2_color[dsk_2_style[desk]],
                concentrate=concentrator,
                desk_name=desk,
            )
        nx.set_node_attributes(gr, self.seat_2_person, "person")
        nx.set_node_attributes(gr, dsk_2_style, "style")
        nx.set_node_attributes(gr, dsk_2_value, "value")
        nx.set_node_attributes(gr, dsk_2_rnk, "rank")

        col_dist = brewer["RdYlGn"][11]
        # max_dist=np.max(np.max(self.distances))
        med_dist = np.median(np.median(self.distances))
        line_size = 5
        for index1, n1 in self.id_2_person.items():
            for index2, n2 in self.id_2_person.items():
                if self.collaboration[index1 - 1][index2 - 1] < collab_thresh:
                    link_trans = 1 - (
                        self.collaboration[index1 - 1][index2 - 1] / collab_thresh
                    )
                    # print("The width is: %f" % link_trans)
                    # print("The score is: %f" % collaboration[index1-1][index2-1])
                    dk1 = self.space_2_index_all[self.person_2_seat[n1]]
                    dk2 = self.space_2_index_all[self.person_2_seat[n2]]
                    dist_value = max(
                        0, 1 - self.distances[dk1][dk2] / (med_dist)
                    )  # modifying the med_dist changes the stricktness for plotting lines based on distance
                    conn = n1 + " to " + n2
                    color_ind = min(
                        10,
                        int(
                            (
                                self.collaboration[index1 - 1][index2 - 1]
                                / (collab_thresh)
                            )
                            * 10
                        ),
                    )
                    gr.add_edge(
                        self.person_2_seat[n1],
                        self.person_2_seat[n2],
                        transp=max(0.2, link_trans * 3),
                        edge_color=col_dist[color_ind],
                        collab=self.collaboration[index1 - 1][index2 - 1],
                        dist=max(0.05, dist_value),
                        size=dist_value * 3,
                        connection=conn,
                    )
                if index1 in self.secretaries:
                    if self.secretaries[index1] == index2:
                        dk1 = self.space_2_index_all[self.person_2_seat[n1]]
                        dk2 = self.space_2_index_all[self.person_2_seat[n2]]
                        dist_value = max(
                            0, 1 - self.distances[dk1][dk2] / (med_dist * 2)
                        )
                        conn = n1 + " to " + n2
                        gr.add_edge(
                            self.person_2_seat[n1],
                            self.person_2_seat[n2],
                            transp=2.5,
                            edge_color="#000000",
                            collab=self.collaboration[index1 - 1][index2 - 1],
                            dist=dist_value,
                            size=line_size,
                            connection=conn,
                        )
        self.gr_plot = gr
        df_s = pd.read_csv(Path.cwd() / dir_name / "spaces_data_coords.csv")
        df_s.columns = ["id", "conc", "value", "x", "y"]

        self.coordinates = {}
        for i in range(len(df_s)):
            self.coordinates[df_s["id"].iloc[i]] = [
                df_s["x"].iloc[i],
                df_s["y"].iloc[i],
            ]
        return

    def plot(self):
        # Check if the solution has been prepared to plot.
        if self.gr_plot is None:
            print(
                "The graph to plot has not been defined. Try running YOUR-OBJECT-NAME.prepare_plot() first."
            )
        else:
            TITLE = "Employee seating arrangement"
            plot = figure(
                plot_width=1000,
                plot_height=1000,
                x_range=Range1d(0, 370),
                y_range=Range1d(0, 250),
            )
            plot.title.text = TITLE

            node_hover_tool = HoverTool(
                tooltips=[
                    ("Employee:", "@person"),
                    ("Desk name:", "@desk_name"),
                    ("Employee rank:", "@rank"),
                    ("Employee style:", "@style"),
                    ("Seat value:", "@value"),
                    ("Concentrating desk:", "@concentrate"),
                ]
            )

            graph_renderer = from_networkx(
                self.gr_plot, nx.spring_layout, scale=0.75, center=(0, 0)
            )
            fixed_layout_provider = StaticLayoutProvider(graph_layout=self.coordinates)
            graph_renderer.layout_provider = fixed_layout_provider

            graph_renderer.node_renderer.glyph = Circle(size=10, fill_color="color")
            graph_renderer.node_renderer.selection_glyph = Circle(
                size=12, fill_color="color"
            )
            graph_renderer.edge_renderer.glyph = MultiLine(
                line_color="edge_color", line_alpha="dist", line_width="size"
            )
            graph_renderer.edge_renderer.selection_glyph = MultiLine(
                line_color="black", line_alpha=0.8, line_width=4
            )
            graph_renderer.selection_policy = NodesAndLinkedEdges()

            plot.renderers.append(graph_renderer)
            plot.add_tools(node_hover_tool, TapTool())

            output_file("Employee_seating_solution_graphic_GreedyAlgorithm.html")
            show(plot)
        return


if __name__ == "__main__":
    pb = SeatingProblem_Greedy(DATA_DIR)
    pb.greedy_params(
        N_ITT=1500,  # the number of LS moves, ~ 0.04 point improvement/swap -- much easier in initial swaps.
        WEIGHTS=[0, 5, 10, 0.1],
    )
    soln_i = pb.run_greedy()
    # soln_i = pb.secretary_swapping(soln_i)
    soln = pb.local_search(soln_i)
    print("\nThe initial fitness is:")
    fitness_i = pb.obj_fn_output(soln_i, report=True)
    print("\nThe final fitness is:\n")
    fitness = pb.obj_fn_output(soln, report=True)
    print("LS reduced the objective by %d" % int(fitness_i - fitness))
    print("LS efficiency is ~ %.3f points / swap." % ((fitness_i - fitness) / pb.N_ITT))
    pb.seat_ranking_check()
    pb.secretary_violation_check()
    pb.save_solution()
    pb.prepare_plot(DATA_DIR)
    pb.plot()
